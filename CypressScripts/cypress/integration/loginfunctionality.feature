Feature:Checking Login functionality of Orange HRM app

        In this file we are checking Login functionality with Valid Invalid Credentials

Scenario: Checking Login functionality with Valid Credentials
    Given Login page of Orange HRM demo application is opened
    When  User enters "Admin" in UserName field
    And   User enters "admin123" in Password field
    And   User clicks on Login button
    Then Welcome screen should appear

Scenario: Checking Login functionality by keeping Username and password blank 
    Given Login page of Orange HRM demo application is opened
    When  User enters "" in UserName field
    And   User enters "" in Password field
    And   User clicks on Login button
    Then Error "Username cannot be empty" should occur

Scenario: Checking Login functionality by entering Username but keeping password blank 
    Given Login page of Orange HRM demo application is opened
    When  User enters "Admin" in UserName field
    And   User enters "" in Password field
    And   User clicks on Login button
    Then Error "Password cannot be empty" should occur

Scenario: Checking Login functionality by keeping Username field blank but entering valid password
    Given Login page of Orange HRM demo application is opened
    When User enters "" in UserName field
    And   User enters "admin123" in Password field
    And   User clicks on Login button
    Then Error "Username cannot be empty" should occur

Scenario: Checking Login functionality with Invalid Credentials
    Given Login page of Orange HRM demo application is opened
    When User enters "Pranoday" in UserName field
    And   User enters "Dingare" in Password field
    And   User clicks on Login button
    Then Error "Invalid credentials" should occur