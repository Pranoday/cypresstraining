///<reference types="cypress" />
describe('Scroll page',()=>{

    it('Scroll till element',()=>{

        cy.visit("https://courses.letskodeit.com/practice")
        //Will align element on TOP of the page
        cy.get("button#mousehover").scrollIntoView()
        //Page will get scrolled down by offset
        //cy.get("button#mousehover").scrollIntoView({ offset: { top: 150, left: 0 } })
        cy.wait(2000)
        cy.get("button#mousehover").trigger("mouseover")
        cy.wait(2000)
        //cy.get("button#mousehover").trigger("mousedown")
        cy.wait(2000)
        cy.get("a[href='#top']").click()

        //cy.get("button#mousehover").scrollIntoView(false)
        //cy.get("button#mousehover").invoke('scrollIntoView')
        /*
        cy.get("button#mousehover").then(($el)=>{
            cy.wrap($el).invoke('scrollIntoView')


        })
        */
    })


})