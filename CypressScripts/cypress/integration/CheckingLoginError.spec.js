///<reference types="cypress" />
describe('Checking Invalid login error',()=>{

    it('Check error when both fields are blank',()=>{

        cy.visit("https://opensource-demo.orangehrmlive.com/")
        cy.get("input#btnLogin").click()
        cy.get("span#spanMessage").then(($el)=>{

            cy.wrap($el).should('have.text',"Username cannot be empty")

        })
     })

     it('Check error when Password field is blank',()=>{
        cy.get("input#txtUsername").type("Admin")
        cy.get("input#btnLogin").click()
        cy.get("span#spanMessage").then(($el)=>{
            
            cy.wrap($el).should('have.text',"Password cannot be empty")

        })

        
     })

     it('Check error when UserName field is blank',()=>{

        cy.get("input#txtUsername").clear()
        cy.get("input#txtPassword").type("admin123")
        cy.get("input#btnLogin").click()
        cy.get("span#spanMessage").then(($el)=>{

              cy.wrap($el).should('have.text',"Username cannot be empty")  

        })
    })

    it('Check error when Invalid credentials are entered',()=>{

        cy.get("input#txtUsername").clear()
        cy.get("input#txtUsername").type("Pranoday")
        cy.get("input#txtPassword").clear()
        cy.get("input#txtPassword").type("Pranoday")
        cy.get("input#btnLogin").click()
        cy.get("span#spanMessage").then(($el)=>{

               cy.wrap($el).should('have.text',"Csrf token validation failed") 

        })
    })

})