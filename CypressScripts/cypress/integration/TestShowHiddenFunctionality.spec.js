///<reference types="cypress" />
//describe block is a suite/collection of test cases
describe('Testing Show/Hide functionality',()=>{

    /*
        Syntax of CSS query is:
        TAGNAME[AttributeName='AttributeValue']
    */
    it('Test Hide functionality',()=>{
        //it block is a test case
        cy.visit("https://courses.letskodeit.com/practice")
        cy.get('input[id=\'hide-textbox\']').click()
        //input[name='show-hide']
        //cy.get("input[name='show-hide']").type("Pranoday")
        //cy.get("input[name='show-hide']").clear()
        cy.get("input[name='show-hide']").should('not.be.visible')


    })

    it('Test Show functionality',()=>{

        cy.get("input[value='Show']").click()
        cy.get("input[name='show-hide']").should('be.visible') 

    })


})