///<reference types="cypress" />
describe('Handling Alert window',()=>{

    it('Handling Alert window',()=>{

        cy.visit("https://courses.letskodeit.com/practice")
        cy.get("input[name='enter-name']").type("Pranoday")
        cy.get("input#alertbtn").click()
        cy.on('window:alert',(AlertTxt)=>{
               expect(AlertTxt).to.contains("Pranoday") 


        })
        cy.on('window:alert', () => true);

    })

})