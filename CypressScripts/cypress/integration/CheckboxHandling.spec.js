///<reference types="cypress" />

describe('Checking Checkboxes',()=>{

    it('Checking if checkbox gets checked',()=>{

        cy.visit("https://courses.letskodeit.com/practice")
        /*
            input#bmwcheck
            input[id='bmwcheck']
        */

        cy.get("input#bmwcheck").click()
        cy.get("input#bmwcheck").should('be.checked')


    })

    it('Checking if checkbox gets unchecked',()=>{

            cy.get("input#bmwcheck").click()
            cy.get("input#bmwcheck").should('not.be.checked')


    })


})