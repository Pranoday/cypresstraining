///<reference types="cypress" />
describe("Checking Hint text",()=>{

    it("Checking hint text",()=>{

        cy.visit("https://courses.letskodeit.com/practice")
        cy.get("input#name").invoke('attr','placeholder').then(HintText=>{

            expect(HintText).to.equal("Enter Your Name")

        })

    })


})