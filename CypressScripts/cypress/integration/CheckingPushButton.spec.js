///<reference types="cypress" />
describe("Checking if push button is in correct state",()=>{

    it("Checking if push button is enabled",()=>{

        cy.visit("https://courses.letskodeit.com/practice")
        cy.get("input#alertbtn").should('be.enabled')
    })

    it("Checking if push button is diabled",()=>{

        cy.get("input#confirmbtn").should('be.disabled')

    })

})