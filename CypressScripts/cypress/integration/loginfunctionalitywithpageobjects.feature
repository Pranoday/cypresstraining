Feature:Checking Login functionality of Orange HRM app with Invalid Credentials

        In this file we are checking Login functionality with Invalid Credentials

Scenario Outline:Checking Login functionality with InValid Credentials
    Given Login page of Orange HRM demo application is opened
    When  User enters '<UserName>' in UserName field
    And   User enters '<Password>' in Password field
    And   User clicks on Login button
    Then  Error '<ErrorMessage>' should be shown
    Examples:
        |UserName|Password|ErrorMessage|
        |Pranoday|Dingare|Invalid credentials|
        | | |Username cannot be empty|
        |Admin||Password cannot be empty|
        |||Username cannot be empty|

    
    