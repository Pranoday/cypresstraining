///<reference types="cypress" />
describe('Handling Confirmation box',()=>{

    it("Handling Confirmation box",()=>{
        
        cy.visit("https://courses.letskodeit.com/practice")
        cy.get("input[name='enter-name']").type("Pranoday")
        cy.get("input[name='enter-name']").invoke('val').then(EnteredName=>{

            cy.get("input#confirmbtn").click()
            cy.on('window:confirm',(ConfirmTxt)=>{
              expect(ConfirmTxt).to.contain(EnteredName)      

            })
            cy.on('window:confirm',()=>false)

        })

    })

})