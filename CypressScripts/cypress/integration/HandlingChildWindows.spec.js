///<reference types="cypress" />
describe("Handling elements from page opened in new tab",()=>{

    it("Clicking on Sign in button from page opened in different tab",()=>{

            cy.visit("https://courses.letskodeit.com/practice")
            cy.get("a#opentab").invoke('removeAttr','target').click()
            cy.get('a[href=\'/login\']').click()
            cy.url().should('include','https://courses.letskodeit.com/login')
            cy.go('back')
            cy.wait(2000)
            cy.go('back')
    })

})