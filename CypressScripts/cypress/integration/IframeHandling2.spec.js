/// <reference types = "Cypress"/>
/// <reference types = "Cypress-iframe"/>
import 'cypress-iframe'
describe("Handling Iframes in Cypress",()=>{

    it('Interactive Textbox and button control from nested page',()=>{

        cy.visit("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_input_test")
        cy.wait(5000)
        cy.frameLoaded('iframe#iframeResult')
        cy.iframe().find('input#fname')
        
    })


})