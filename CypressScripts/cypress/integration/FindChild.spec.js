///<reference types="cypress" />
describe("Demonstrating findchild",()=>{


    it('Understanding when to use FindChild',()=>{

        cy.visit("https://courses.letskodeit.com/practice")
        
        cy.get('div#checkbox-example').find('input[value=\'benz\']').click()
        cy.get('div#checkbox-example').find('input[value=\'benz\']').should('be.checked')
    })

})