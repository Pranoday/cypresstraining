/// <reference types = "Cypress"/>
/// <reference types = "Cypress-iframe"/>
import 'cypress-iframe'
describe("Handling Iframes in Cypress",()=>{

    it('Interactive Textbox and button control from nested page',()=>{

        cy.visit("https://courses.letskodeit.com/practice")
        cy.frameLoaded('#courses-iframe')
        /*
        cy.iframe().find('a').each(($el, index, $list)=>{

                cy.wrap($list[0]).click()

        })
        */
        cy.iframe().find('a[href=\'/login\']').click()
        cy.wait(2000)
        cy.frameLoaded('#courses-iframe')
        cy.iframe().find('input[placeholder="Email Address"]').type("pranoday.dingare@gmail.com")
        cy.iframe().find('input[value=\'Login\']').click()

    })


})