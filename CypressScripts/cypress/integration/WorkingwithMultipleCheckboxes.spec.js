///<reference types="cypress" />

describe('Working with multiple controls',()=>{

    it("Clicking on multiple checkboxes",()=>{

        cy.visit("https://courses.letskodeit.com/practice")
        cy.get('div#checkbox-example').find('input[type=checkbox]').each(($el, index, $list)=>{

            cy.wrap($el).click()
            cy.wrap($el).should('be.checked')
            
        })

    })

    it("Clicking on checked checkboxes and checking their status",()=>{

        cy.get('div#checkbox-example').find('input[type=checkbox]').each(($el, index, $list)=>{
            cy.wrap($el).click()
            cy.wrap($el).should('not.be.checked')
            
        })

    })

})