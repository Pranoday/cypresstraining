///<reference types="cypress" />
describe("Checking behaviour of Textbox control",()=>{

 it("Checking text is getting typed in",()=>{

    cy.visit("https://courses.letskodeit.com/practice")
    cy.get("input[placeholder='Hide/Show Example']").type("Pranoday")
    cy.get("input[placeholder='Hide/Show Example']").should('have.value',"Pranoday")


 })

 it("Checking newly typed value is getting appended",()=>{
    cy.get("input[placeholder='Hide/Show Example']").type("Dingare")
    cy.get("input[placeholder='Hide/Show Example']").should('have.value',"PranodayDingare")

 })

 it("checking text is getting cleared",()=>{

        cy.get("input[placeholder='Hide/Show Example']").clear()
        cy.get("input[placeholder='Hide/Show Example']").should('have.value',"")
 })

})