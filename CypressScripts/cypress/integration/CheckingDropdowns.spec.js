///<reference types="cypress" />
describe("Checking if dropdowns are working fine",()=>{

   it("Checking if options are getting selected",()=>{

        cy.visit("https://courses.letskodeit.com/practice")
        cy.get("select#carselect").should('have.value','bmw')
        cy.get("select#carselect").select("Benz")
        //cy.get('option[value="bmw"]').should('be.selected');
        cy.get("select#carselect").select("BMW")
        cy.get("select#carselect").should('have.value','bmw')
        cy.get('option[value="bmw"]').should('be.selected');
 

   })

   it('Changing selection and checking that new value is selected',()=>{

      cy.get("select#carselect").select("Honda")
      cy.get("select#carselect").should('have.value','honda')

   })

})