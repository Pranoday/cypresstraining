///<reference types="cypress" />
class LoginPage
{
    EnterUserName(UserName)
    {
        //cy.get("input#txtUsername").type(UserName)
        cy.get('input#txtUsername').clear()
        cy.get('input#txtUsername').invoke('val', UserName)      
    }
    EnterPassword(Password)
    {
        //cy.get("input#txtPassword").type(Password)
        cy.get("input#txtPassword").clear()
        cy.get('input#txtPassword').invoke('val', Password)      
    }

    ClickLoginButton()
    {
        cy.get("input#btnLogin").click()
    }

    ReturnLoginErrorMessage()
    {
        cy.get("span#spanMessage").then(($el)=>{

            ElementText=$el.text()
            cy.log(ElementText)
            return ElementText
        })
        
    }

    DoLogin(UserName,Password)
    {
        this.EnterUserName(UserName)
        this.EnterPassword(Password)
        this.ClickLoginButton()
    }
}
export default LoginPage