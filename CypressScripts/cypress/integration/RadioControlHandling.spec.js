///<reference types="cypress" />

describe('Checking radio controls are working fine',()=>{

    it('Checking radio control is getting checked',()=>
    
        {
                cy.visit("https://courses.letskodeit.com/practice")
                cy.get("input#bmwradio").click()
                cy.get("input#bmwradio").should('be.checked')

        }
    )

    it('checking another radio control is getting properly checked',()=>{
        cy.get('input#benzradio').click()
        cy.get('input#benzradio').should('be.checked')
        cy.get("input#bmwradio").should('not.be.checked')
    }) 

})