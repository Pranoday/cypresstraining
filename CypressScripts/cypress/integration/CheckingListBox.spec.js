///<reference types="cypress" />
describe("Checking if Listbox is working fine",()=>{

    it("Check if value is getting selected",()=>{

        cy.visit("https://courses.letskodeit.com/practice")
        cy.get("select#multiple-select-example").select("Orange")
        cy.get("select#multiple-select-example").select("Peach")
        cy.get('option[value="peach"]').should('be.selected');
        //Checking have.value does not work on listboxes
        //cy.get("select#multiple-select-example").should('have.value','peach')
    })

    it("Check of multiselection works",()=>{
        
        //Multiselection
        cy.get('select#multiple-select-example').select(['Orange','Apple'])
        cy.get('select#multiple-select-example').find('option').each(($el, index, $list)=>{

               if(cy.wrap($el).selected)
               {
                   console.log(index)
                   cy.log(index) 
                } 
                if($list.length==3)
                {
                    console.log("Correct number of options are present in Listbox")
                }
        })
    })
})