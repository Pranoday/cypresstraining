///<reference types="cypress" />
import { Given , And , Then , When} from "cypress-cucumber-preprocessor/steps";
//import {LoginPage} from "../PageObjects/LoginPage"
const LoginPage = require('../PageObjects/LoginPage');
var loginpage=null
Given("Login page of Orange HRM demo application is opened",()=>{

    cy.visit("https://opensource-demo.orangehrmlive.com/")
    loginpage=new LoginPage()
})

When("User enters {string} in UserName field",(UserName)=>{

        
//cy.get("input#txtUsername").type(UserName)

//cy.get('input#txtUsername').invoke('val', UserName)      
  loginpage.EnterUserName(UserName)
})

/*When("User enters Password in Password field",(dataTable)=>{
    //cy.get("input#txtPassword").type(Password)
    var propValue;
    dataTable.hashes().forEach(elem =>{
    for(var propName in elem) {
        propValue = elem[propName]
        cy.get("input#txtPassword").type(propValue)
        cy.log(propName,propValue);
    }
    })
})
*/
When("User enters {string} in Password field",(Password)=>{
    //cy.get("input#txtPassword").type(Password)
    //cy.get('input#txtPassword').invoke('val', Password)      
    loginpage.EnterPassword(Password)
    
})
When("User clicks on Login button",()=>{
    //cy.get("input#btnLogin").click()
    loginpage.ClickLoginButton()
})

Then("Error {string} should be shown",(Error)=>{
    
    var LoginError=loginpage.ReturnLoginErrorMessage()
    

        //expect(LoginError).contain(Error)
        expect(LoginError).to.equal(Error)
})
    


