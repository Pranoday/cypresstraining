///<reference types="cypress" />
import { Given , And , Then , When} from "cypress-cucumber-preprocessor/steps";


Given("Login page of Orange HRM demo application is opened",()=>{

    cy.visit("https://opensource-demo.orangehrmlive.com/")

})

When("User enters {string} in UserName field",(UserName)=>{
  //When("User enters \"Admin\" in UserName field",()=>{
        cy.get("input#txtUsername").clear()
        //cy.get("input#txtUsername").type("Admin")
        cy.get('input#txtUsername').invoke('val', UserName)      

    })

When("User enters {string} in Password field",(Password)=>{
    cy.get("input#txtPassword").clear()
    cy.get('input#txtPassword').invoke('val', Password)      

})

When("User clicks on Login button",()=>{
    cy.get("input#btnLogin").click()

})

Then("Welcome screen should appear",()=>{

    cy.wait(10000)
    cy.url().should('eq','https://opensource-demo.orangehrmlive.com/index.php/dashboard')
    

})
    
Then("Error {string} should occur",(Error)=>{

    cy.get("span#spanMessage").then(($el)=>{
            
        cy.wrap($el).should('have.text',Error)

    })

})


