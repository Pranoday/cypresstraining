/*
    Switch statement helps in comparing input with various values without getting into
    complicated code having if,elseif,else

    switch statement runs matched case along with all cases present after the matched one.
    We need to use break if we want to run only single matched case.  
*/
var MonthNumber=3
switch(MonthNumber)
{
    case 1:
        console.log("JAN")
        break
    case 2:
        console.log("FEB") 
        break
    case 3:
        console.log("MAR") 
        break
    case 4:
        console.log("APR") 
        break
}