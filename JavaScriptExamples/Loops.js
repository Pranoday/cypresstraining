//For loop
console.log("Printing nunnbers 1 to 10 using for loop")
for(let i=1;i<=10;i++)
{
    console.log(i)

}

//While loop
var Number=9,i=1,count=0
while(i<=Number)
{
    if(Number%i==0)
    {
           count++ 
    }
    i++
}

if(count==2)
{
    console.log("Number : "+Number+" is prime")
}
else
{
    console.log("Number : "+Number+" is NOT prime")
}

//for in loop
Person={"Name":"Pranoday","LastName":"Dingare","Experience":10}
for (let property in Person)
{
    console.log(Person[property])
}

console.log("Printing array members using Normal for loop")
//for in used with Arrays
Numbers=[10,20,30,40,50]
for(let index=0;index<Numbers.length;index++)
{
    console.log(Numbers[index])

}

console.log("Printing array members using  for in loop")
for(let Number in Numbers)
{
    console.log(Numbers[Number])
}

/*
    forEach() will call a function defined for every element in an Array and will send array 
    element,its index and array as an argument to that function. 
*/
const numbers = [45, 4, 9, 16, 25];

let txt = "";
numbers.forEach(myFunction);

function myFunction(value, index, array) {
  console.log(value+" "+index)
}

//for of loop
/*
    Difference between "for of" AND "for in"

    for in loop when used to iterate over array,returns index of an array element loop is 
    iterating over
    
    for of loop when used to iterate over array,returns  an array element loop is iterating over
 */
const cars = ["BMW", "Volvo", "Mini"];
let text = "";

for (let x of cars) {
  console.log(x)
}

var Name="Pranoday"
console.log("Printing characters from string using for of loop")
for (let char of Name)
{
    console.log(char)
}
console.log("Printing characters from string using for in loop")
for(let charidx in Name)
{
       console.log(Name[charidx])
} 

//Do while loop
var i=1
do
{
    console.log(i)
    i++
}while(i<=10)