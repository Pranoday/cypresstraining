/*
    Arrays can be initialized with comma separated values mentioned in SQUARE brackets i.e.[]
    Array members can be accessed using 0-based index.
*/
cars=["Saab","BMW","Honda"]
console.log(cars[0])

//If we try to access Non-existing element from an array then we get 'undefined'
console.log(cars[10])

/*
    If const is applied to an Array variable then:
    1.We can change the individual array elements BUT
    2.We can not assign any other array to this const variable
*/
const cars1=["Saab","BMW","Honda"]
cars1[0]="KIA"
console.log(cars1[0])

cars1=["Mahindra","TATA"]