/*
    A function in JS is a reusable block.
    If we want to send any values to function while calling, we need parameters.
    We do not need to mention return type while defining function in JS
*/

function Addition(num1,num2)
{

    Res=num1+num2
    console.log(Res)
}
Addition(10,20)

// For returning value from function we need to use 'return' statement
function FullName(FirstName,LastName)
{
    //Here '+' as it is operating on string type values,working as a Contcatenation operator 
    MyFullName=FirstName+" "+LastName
    return MyFullName
}
FullNameReturned=FullName("Pranoday","Dingare")
console.log(FullNameReturned)

function Subtraction(num1,num2)
{
    return num1-num2
}

console.log("Subtracting "+10+" from 20,result will be "+Subtraction(20,10))

function Multiplication(num1,num2)
{
    /*
        Variable defined inside function using let keyword is a local variable to the function.
        And it is not available outside of a function block
    */
    let MultiplicationResult=num1*num2
    return MultiplicationResult
}
