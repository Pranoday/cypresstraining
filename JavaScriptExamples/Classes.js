class Account
{
    constructor(ActNo,Bal)
    {
            this.AccountNo=ActNo
            this.Balance=Bal
    }

    WithdrawAmount(AmountTobeWithdrawn) {

            this.Balance=this.Balance-AmountTobeWithdrawn
        
    }

    GetBalance()
    {
        return this.Balance
    }

}

let Act1=new Account(1,1000)
console.log("Initial balance is "+Act1.GetBalance())
Act1.WithdrawAmount(200)
console.log("Balance after withdrawal is "+Act1.GetBalance())

let Act2=new Account(2,2000)
console.log("Initial balance is "+Act2.GetBalance())
Act2.WithdrawAmount(200)
console.log("Balance after withdrawal is "+Act2.GetBalance())
