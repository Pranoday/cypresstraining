var X=5
var Y=10
var Z=0

Z=X+Y
console.log(Z)

FirstName="Pranoday"
LastName="Dingare"
FullName=FirstName+LastName
console.log(FullName)

 let name1="Pranoday"
 /*
    Variable defined using let can not be redeclared.name1 variable is declared using let.
    If we try to redeclare it,say with different value.It will generate an error.
 */
 //let name1=10
console.log(name1)

if(1<2)
{

    /*
        let allows us to create block scoped variables. In this example we have created a variable
        named Variable1 inside if block.This variable will be available only within if block.
        If we define this variable WITHOUT let keyword then this variable will be available
        also outside of if block
    */
    let Variable1=2;    
    console.log(Variable1)
}
Variable1=20
console.log(Variable1)

if(1<2)
{
    /*
        Variables defined using var keyword are available also outside of the block
    */

    var Variable2="PD"
    console.log(Variable2)
}
console.log(Variable2)

/*
    Redeclaring means we are defining variable again with new value where OLD value gets replaced
    with NEW value
*/
var R=10
if(1<2)
{
    var R=20
    console.log(R)
}
console.log(R)

/*
    In example below there are 2 different variables with name 'L' are getting created.
    First variable L is available OUT of the block.
    
    2nd variable 'L' is available only inside the if block
*/

let L=100
if(1<2)
{
    let L=200
    console.log(L)
}
console.log(L)

/*
    
    Variables defined with const are FINAL variable.i.e. 
    1.We need to compulsarily INITIALIZE them.
    2.We can not change value of const variable
*/
const C=1000
C=2000
console.log(C)