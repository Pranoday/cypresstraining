/*
    Object in JS:
    It is a collection of Key-Value pair.
    All Key-value pairs are called as properties.
    If we want to access any property then SYNTAX-> ObjectVariable.propertyName
*/

/*
    If we defined a JS object with const then:
    1.We can change the value of existing property BUT
    2.We can not use same variable to store another object.
*/
const Person={"Name":"Pranoday","SirName":"Dingare","Age":20}
console.log(Person.Name)
Person.Name="PD"
console.log(Person.Name)

Person={"Name":"Parag","SirName":"Dingare","Age":30}

