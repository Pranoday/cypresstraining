let carName1 = "Volvo XC60";  // Double quotes
let carName2 = 'Volvo XC60';  // Single quotes

console.log(carName1.length)
console.log("\"Pranoday\"")
console.log('\'Pranoday\'')
console.log("'Pranoday'")
console.log('"Pranoday"')

let x = "John";
let y = new String("John");
console.log(y)
